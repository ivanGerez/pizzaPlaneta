
package pizzaplaneta;

import java.util.ArrayList;
import pizzaplaneta2.VentanaFormulario;
import pizzaplaneta2.VentanaProductos;
import pizzaplaneta3.Categoria;
import pizzaplaneta3.Producto;
import pizzaplaneta4.CategoriasDao;
import pizzaplaneta4.ProductosDao;


public class ControladorProductos {
    private VentanaProductos ventana;
    private VentanaFormulario formulario;
    private ArrayList <Producto> productos = ProductosDao.getProductos();
    private ArrayList<Categoria> categorias = CategoriasDao.getCategorias();
    
    public void iniciar(){ 
        ventana = new VentanaProductos(this);
        productos =ProductosDao.getProductos();
        categorias = CategoriasDao.getCategorias();
        cargarProductos();
        actualizarListado();
        ventana.setVisible(true);
    }
    public void cargarProductos ()
    {
      Categoria cat1 = new Categoria ("Bebidas");
       categorias.add(cat1);
      Categoria  cat2 =  new Categoria ("Pizza Especial");
      categorias.add(cat2);
      Categoria cat3 = new Categoria ("Pizza Comun");
      categorias.add(cat3);
      //Productos
      Producto prod1 = new Producto ( "Coca cola 1.5L", 60, true, cat1);
      Producto prod2 = new Producto ( "Pizza con morrones y jamon cocido ", 150, true, cat2);
      Producto prod3 = new Producto ( "Fanta 1.5L", 60, true, cat1);
      Producto prod4 = new Producto ( "Pizza Comun", 100, true, cat3);
      Producto prod5 = new Producto ( "Pizza de Panceta y muzzarella", 160, true, cat2);
      Producto prod6 = new Producto ( "Pepsi 1.5L", 60, true, cat1);
      Producto prod7 = new Producto ( "Paso de los Toros 1.5L", 60, true, cat1);
      Producto prod8 = new Producto ( "Pizza Muzarella Napolitana", 110, true, cat3);
    
    }private void actualizarListado(){
        ventana.llenarTabla(productos);
    }
    
    // Se llama cuando el usuario clickea "Eliminar" en la ventana de listado
    public void eliminar(int posicionSeleccionada) {
        // "remove" es un método con el que ya viene en ArrayList,
        // Elimina el elemento situado en la posicion que se pasa.
        Producto productoSeleccionado = productos.remove(posicionSeleccionada);
        actualizarListado();
    }

    // Se llama cuando el usuario clickea "Crear" en la ventana de listado
    // Abre el formulario para crear
    public void crear() {
        VentanaFormulario formulario = new VentanaFormulario(this);
        formulario.setCategorias(categorias); // Para llenar el combo box
        formulario.setProducto(null); // Para indicar que se está creando, no editando.
        formulario.setVisible(true);
    }

    // Se llama cuando el usuario presiona "Guardar" en el formulario, 
    // cuando se está creando un nuevo producto
    public void crear( Categoria categoria, String nombre, double precio, boolean disponible) {
        Producto productoNuevo = new Producto( nombre, precio, disponible, categoria);
        productos.add(productoNuevo);
        actualizarListado();
    }

    // Se llama cuando el usuario clickea "Editar" en la ventana de listado
    // Abre el formulario para editar
    public void editar(int posicionSeleccionada) {
        // obtiene el producto elegido para editar
        // a partir de la posición seleccionada
        Producto productoSeleccionado = productos.get(posicionSeleccionada);
        
        formulario.setCategorias(categorias); // Para llenar el combo box
        formulario.setProducto(productoSeleccionado); // Para indicar que se está editando ese producto
        formulario.setVisible(true);
    }  

    // Se llama cuando el usuario presiona "Guardar" en el formulario, 
    // cuando se está editando un producto
    public void editar(Producto productoEditado , Categoria categoria, String nombre, double precio, boolean disponible) {
        productoEditado.actualizarAtributos( categoria, nombre, precio, disponible);
        actualizarListado();
    }
}

