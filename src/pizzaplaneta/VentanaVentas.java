
package pizzaplaneta;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;


public class VentanaVentas extends javax.swing.JFrame {
     private ControladorVentas controlador;
    
    public VentanaVentas(ControladorVentas controlador) {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/pizzaplaneta/pizza.jpg")).getImage());
        ((JPanel)getContentPane()).setOpaque(false);
        ImageIcon uno=new ImageIcon(this.getClass().getResource("/pizzaplaneta/pizza2.jpeg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        fondo.setBounds(0,0,uno.getIconWidth(),uno.getIconHeight());
    this.controlador = controlador;
        setLocationRelativeTo(null);
        setTitle("LuchoPizzas!");
        lblTotal.setText("");
        lblTurno.setText("");
        lblRecaudacion.setText("");
    }
    
    public void gasto(double gasto, int turno, double recaudado){
        String gastoTotal = Double.toString(gasto);
        lblTotal.setText("Total: $" +gastoTotal);
        String turnoDelPedido = Integer.toString(turno);
        lblTurno.setText("Turno Nº: " + turnoDelPedido);
        String recaudacionDelDia = Double.toString(recaudado);
        lblRecaudacion.setText("Recaudación del día: $" + recaudacionDelDia);
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtLomito = new javax.swing.JTextField();
        lblTurno = new javax.swing.JLabel();
        lblHamburguesa = new javax.swing.JLabel();
        btnNuevoPedido = new javax.swing.JButton();
        txtHamburguesa = new javax.swing.JTextField();
        lblRecaudacion = new javax.swing.JLabel();
        lblPapas = new javax.swing.JLabel();
        txtPapas = new javax.swing.JTextField();
        lblGaseosaChica = new javax.swing.JLabel();
        txtGaseosaChica = new javax.swing.JTextField();
        lblGaseosaGrande = new javax.swing.JLabel();
        lblMilanesa = new javax.swing.JLabel();
        txtGaseosaGrande = new javax.swing.JTextField();
        txtPizzaComun = new javax.swing.JTextField();
        btnPedido = new javax.swing.JButton();
        lblLomito = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txtLomito.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtLomito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLomitoActionPerformed(evt);
            }
        });

        lblTurno.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTurno.setText("Turno");

        lblHamburguesa.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblHamburguesa.setText("EspecialLucho:");

        btnNuevoPedido.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnNuevoPedido.setForeground(new java.awt.Color(51, 51, 51));
        btnNuevoPedido.setText("Nuevo pedido");
        btnNuevoPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoPedidoActionPerformed(evt);
            }
        });

        txtHamburguesa.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtHamburguesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHamburguesaActionPerformed(evt);
            }
        });

        lblRecaudacion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblRecaudacion.setText("Recaudacion del día");

        lblPapas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblPapas.setText("Papas Lucho :");

        txtPapas.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPapas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPapasActionPerformed(evt);
            }
        });

        lblGaseosaChica.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblGaseosaChica.setText("Gaseosa chica:");

        txtGaseosaChica.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtGaseosaChica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGaseosaChicaActionPerformed(evt);
            }
        });

        lblGaseosaGrande.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblGaseosaGrande.setText("Gaseosa grande:");

        lblMilanesa.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblMilanesa.setText("PizzaComun:");

        txtGaseosaGrande.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtGaseosaGrande.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGaseosaGrandeActionPerformed(evt);
            }
        });

        txtPizzaComun.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPizzaComun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPizzaComunActionPerformed(evt);
            }
        });

        btnPedido.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnPedido.setForeground(new java.awt.Color(51, 51, 51));
        btnPedido.setText("Tomar pedido");
        btnPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPedidoActionPerformed(evt);
            }
        });

        lblLomito.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblLomito.setText("Pizza Especial:");

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotal.setText("Total");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(55, 55, 55)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblGaseosaGrande, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblMilanesa, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblLomito, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblHamburguesa, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPapas, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblGaseosaChica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(btnPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnNuevoPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(4, 4, 4)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtPizzaComun, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblRecaudacion)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtLomito)
                            .addComponent(txtHamburguesa)
                            .addComponent(txtPapas)
                            .addComponent(txtGaseosaChica)
                            .addComponent(txtGaseosaGrande)
                            .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(lblTurno))
                    .addContainerGap(55, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 528, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMilanesa)
                        .addComponent(txtPizzaComun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(30, 30, 30)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblLomito)
                        .addComponent(txtLomito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(56, 56, 56)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblHamburguesa)
                        .addComponent(txtHamburguesa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(32, 32, 32)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblPapas)
                        .addComponent(txtPapas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(33, 33, 33)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblGaseosaChica)
                        .addComponent(txtGaseosaChica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(34, 34, 34)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblGaseosaGrande)
                        .addComponent(txtGaseosaGrande, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(47, 47, 47)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTotal))
                    .addGap(29, 29, 29)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblTurno)
                            .addGap(26, 26, 26)
                            .addComponent(lblRecaudacion)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(24, 24, 24)
                            .addComponent(btnNuevoPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap()))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtLomitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLomitoActionPerformed
        notificar();
    }//GEN-LAST:event_txtLomitoActionPerformed
   
    private void btnNuevoPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoPedidoActionPerformed

        txtPizzaComun.setText("");
        txtLomito.setText("");
        txtHamburguesa.setText("");
        txtPapas.setText("");
        txtGaseosaChica.setText("");
        txtGaseosaGrande.setText("");

    }//GEN-LAST:event_btnNuevoPedidoActionPerformed

    private void txtHamburguesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHamburguesaActionPerformed
        notificar();
    }//GEN-LAST:event_txtHamburguesaActionPerformed

    private void txtPapasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPapasActionPerformed
        notificar();
    }//GEN-LAST:event_txtPapasActionPerformed

    private void txtGaseosaChicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGaseosaChicaActionPerformed
        notificar();
    }//GEN-LAST:event_txtGaseosaChicaActionPerformed

    private void txtGaseosaGrandeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGaseosaGrandeActionPerformed
        notificar();
    }//GEN-LAST:event_txtGaseosaGrandeActionPerformed

    private void txtPizzaComunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPizzaComunActionPerformed
        notificar();
    }//GEN-LAST:event_txtPizzaComunActionPerformed

    private void btnPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPedidoActionPerformed

        notificar();

    }//GEN-LAST:event_btnPedidoActionPerformed
   public int getValorEntero(String texto){
         if (texto.isEmpty()){
            return 0;
        }
        else{
            return Integer.parseInt(texto);
        }
    }
    public void notificar()
  {
   int milanesas = getValorEntero(txtPizzaComun.getText());
        int lomitos = getValorEntero(txtLomito.getText());
        int hamburguesas = getValorEntero(txtHamburguesa.getText());
        int papas = getValorEntero(txtPapas.getText());
        int gaseosaChica = getValorEntero(txtGaseosaChica.getText());
        int gaseosaGrande = getValorEntero(txtGaseosaGrande.getText());
        controlador.calcular(milanesas , lomitos , hamburguesas , papas, gaseosaChica, gaseosaGrande);
                                             

  }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNuevoPedido;
    private javax.swing.JButton btnPedido;
    private javax.swing.JLabel lblGaseosaChica;
    private javax.swing.JLabel lblGaseosaGrande;
    private javax.swing.JLabel lblHamburguesa;
    private javax.swing.JLabel lblLomito;
    private javax.swing.JLabel lblMilanesa;
    private javax.swing.JLabel lblPapas;
    private javax.swing.JLabel lblRecaudacion;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTurno;
    private javax.swing.JTextField txtGaseosaChica;
    private javax.swing.JTextField txtGaseosaGrande;
    private javax.swing.JTextField txtHamburguesa;
    private javax.swing.JTextField txtLomito;
    private javax.swing.JTextField txtPapas;
    private javax.swing.JTextField txtPizzaComun;
    // End of variables declaration//GEN-END:variables
}
