
package pizzaplaneta;

import java.util.ArrayList;


public class ControladorVentas {
    
    private VentanaVentas ventana;
    private Pedido pedido;
    private ArrayList<Pedido> pedidos = new ArrayList();
        
    public void iniciar(){
        
        ventana = new VentanaVentas(this);
        ventana.setVisible(true);
        
        
    }
    
    public void calcular(int milanesas, int lomitos, int hamburguesas, int papas, int gasChicas, int gasGrandes){
         
        pedido = new Pedido(milanesas, lomitos, hamburguesas, papas, gasChicas, gasGrandes, 90, 105, 85, 50, 40, 70);
        pedidos.add(pedido);  
        double recaudacion = 0;
        for(int i=0; i< pedidos.size(); i++){
            recaudacion = recaudacion + pedidos.get(i).getGasto();
            
        }
        ventana.gasto(pedido.getGasto(), pedidos.size(), recaudacion);
                        
    }
    
    
            
    
}
