
package pizzaplaneta;


public class Pedido {
    
    private int cantidadMilanesas;
    private int cantidadLomitos;
    private int cantidadHamburguesas;
    private int cantidadPapas;
    private int cantidadGasChica;
    private int cantidadGasGrande;
    private double precioMilanesas;
    private double precioLomitos;
    private double precioHamburguesa;
    private double precioPapas;
    private double precioGasChica;
    private double precioGasGrande; 

    public Pedido(int cantidadMilanesas, int cantidadLomitos, int cantidadHamburguesas, int cantidadPapas, int cantidadGasChica, int cantidadGasGrande, double precioMilanesas, double precioLomitos, double precioHamburguesa, double precioPapas, double precioGasChica, double precioGasGrande) {
        this.cantidadMilanesas = cantidadMilanesas;
        this.cantidadLomitos = cantidadLomitos;
        this.cantidadHamburguesas = cantidadHamburguesas;
        this.cantidadPapas = cantidadPapas;
        this.cantidadGasChica = cantidadGasChica;
        this.cantidadGasGrande = cantidadGasGrande;
        this.precioMilanesas = precioMilanesas;
        this.precioLomitos = precioLomitos;
        this.precioHamburguesa = precioHamburguesa;
        this.precioPapas = precioPapas;
        this.precioGasChica = precioGasChica;
        this.precioGasGrande = precioGasGrande;
    }

    public double getGasto(){
        double gasto = (cantidadMilanesas * precioMilanesas) + (cantidadLomitos * precioLomitos) + (cantidadHamburguesas * precioHamburguesa) + (cantidadPapas * precioPapas) + (cantidadGasChica * precioGasChica) + (cantidadGasGrande * precioGasGrande);
        return gasto;
    }
    
}
