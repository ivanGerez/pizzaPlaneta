
package pizzaplaneta3;


public class Categoria {
  private String nombre;

    public Categoria(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    // Define lo que se muestra en el combobox
    @Override
    public String toString() {
        return nombre;
    } 
}
