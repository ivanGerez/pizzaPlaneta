
package pizzaplaneta3;


public class Producto {
    private String nombre;
    private double precio;
    private boolean disponible;
    private Categoria categoria;

    public Producto(String nombre, double precio, boolean disponible, Categoria categoria) {
        
        this.nombre = nombre;
        this.precio = precio;
        this.disponible = disponible;
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    // Actualiza el valor de todos los atributos en un sólo método
    // Es como un gran setter
    public void actualizarAtributos( Categoria categoria, String nombre, double precio, boolean disponible) {
        this.categoria = categoria;
        this.nombre = nombre;
        this.precio = precio;
        this.disponible = disponible;
    }
    
}