
package pizzaplaneta2;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import pizzaplaneta3.Producto;

    
public class ProductosTableMode extends AbstractTableModel {

    private ArrayList<Producto> productos;
    private String[] columnas = {  "Categoría", "Nombre", "Precio", "Disponible" };

    public ProductosTableMode(ArrayList<Producto> productos) {
        this.productos = productos;
    }
    
    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public int getRowCount() {
        return productos.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        Producto producto = productos.get(rowIndex);
        
        switch(columnIndex){
            case 0:
               
                return producto.getCategoria().getNombre();
            case 1:
                return producto.getNombre();
            case 2:
                return producto.getPrecio();
            case 3:
                if(producto.isDisponible()){
                    return "Sí";
                }else{
                    return "No";
                }
        }
        return null;
    }
    
}


